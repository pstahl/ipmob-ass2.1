package com.example.philip.ass21;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Random;


public class MainActivity extends Activity {

    private Long prime;
    private final String filename = "primefile";
    private final String PRIME = "PRIME";
    private TextView primeValue;
    private File file;
    private Handler handler;
    private PrimeRunnable primeRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        primeValue = (TextView) findViewById(R.id.primeValue);
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/primes");
        file = new File(dir, filename);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                Log.d("onCreate", "Failed to create file.");
            }
        }
        loadPrime();
        primeRunnable = new PrimeRunnable();
        handler = new Handler();
        new Thread(primeRunnable).start();
    }


    private class PrimeRunnable implements Runnable {
        private Object mPauseLock;
        private boolean mPaused;
        private boolean mFinished;

        public PrimeRunnable() {
            mPauseLock = new Object();
            mPaused = false;
            mFinished = false;
        }

        public void run() {
            while (!mFinished) {
                prime = getNextPrime();
                updatePrimeText();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    Log.d("", "");
                }
                synchronized (mPauseLock) {
                    while (mPaused) {
                        try {
                            mPauseLock.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }
        }

        public void onPause() {
            synchronized (mPauseLock) {
                mPaused = true;
            }
        }

        public void onResume() {
            synchronized (mPauseLock) {
                mPaused = false;
                mPauseLock.notifyAll();
            }
        }

        private void updatePrimeText() {
            handler.post(new Runnable() {
                public void run() {
                    primeValue.setText("" + prime);
                }
            });
        }

        private Long getNextPrime() {
            Long nextPrime = prime + 2;
            while (!isPrime(nextPrime)) {
                nextPrime += 2;
            }
            return nextPrime;
        }

        private Boolean isPrime(Long candidate) {
            double sqrt = Math.sqrt(candidate.longValue());
            for (long i = 3; i <= sqrt; i += 2)
                if (candidate % i == 0) return false;
            return true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("onResume", "Loading prime from file.");
        loadPrime();
        primeRunnable.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        primeRunnable.onPause();
        Log.d("onPause", "Prime: " + prime);
        try {
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            out.writeLong(prime);
            out.close();
        } catch (IOException e) {
            Log.d("onPause", "Failed to write to file: " + e.toString());

        }
        primeValue.setText("" + prime);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        prime = savedInstanceState.getLong(PRIME);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putLong(PRIME, prime);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    private void loadPrime() {
        try {
            DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
            prime = in.readLong();
            in.close();
        } catch (FileNotFoundException e) {
            Log.d("loadPrime", "Failed to load prime from file: " + e.toString());
        } catch (IOException e) {
            Log.d("loadPrime", "Failed to load prime from file: " + e.toString());
        } finally {
            if (prime == null) {
                prime = new Long(3);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so Long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
